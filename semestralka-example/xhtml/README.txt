CMD(xalan): java org.apache.xalan.xslt.Process -in ../xml-template.xml -xsl xhtml-transformation.xsl -out xhtml-output.html

CMS(saxon): java net.sf.saxon.Transform -dtd:on -s:../xml-template.xml -xsl:xhtml-transformation.xsl -o:xhtml-output.html (with DTD validation)