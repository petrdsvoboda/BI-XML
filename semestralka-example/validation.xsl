<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sch="http://www.ascc.net/xml/schematron"
                version="1.0">
   <xsl:output method="text"/>
   <xsl:template match="*|@*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:if test="count(. | ../@*) = count(../@*)">@</xsl:if>
      <xsl:value-of select="name()"/>
      <xsl:text>[</xsl:text>
      <xsl:value-of select="1+count(preceding-sibling::*[name()=name(current())])"/>
      <xsl:text>]</xsl:text>
   </xsl:template>
   <xsl:template match="/">
      <xsl:apply-templates select="/" mode="M0"/>
      <xsl:apply-templates select="/" mode="M1"/>
      <xsl:apply-templates select="/" mode="M2"/>
      <xsl:apply-templates select="/" mode="M3"/>
      <xsl:apply-templates select="/" mode="M4"/>
      <xsl:apply-templates select="/" mode="M5"/>
      <xsl:apply-templates select="/" mode="M6"/>
      <xsl:apply-templates select="/" mode="M7"/>
      <xsl:apply-templates select="/" mode="M8"/>
      <xsl:apply-templates select="/" mode="M9"/>
      <xsl:apply-templates select="/" mode="M10"/>
      <xsl:apply-templates select="/" mode="M11"/>
      <xsl:apply-templates select="/" mode="M12"/>
      <xsl:apply-templates select="/" mode="M13"/>
      <xsl:apply-templates select="/" mode="M14"/>
      <xsl:apply-templates select="/" mode="M15"/>
   </xsl:template>
   <xsl:template match="/notebooks" priority="4000" mode="M0">
      <xsl:if test="count(pc) &lt; 1">In pattern count(pc) &lt; 1:
   element notebooks musí obsahovat alespoň jednu položku pc
</xsl:if>
      <xsl:apply-templates mode="M0"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M0"/>
   <xsl:template match="/notebooks/pc" priority="4000" mode="M1">
      <xsl:if test="count(name) != 1">In pattern count(name) != 1:
   element pc musí obsahovat právě jednu položku name
</xsl:if>
      <xsl:if test="count(manufacturer) != 1">In pattern count(manufacturer) != 1:
   element pc musí obsahovat právě jednu položku manufacturer
</xsl:if>
      <xsl:if test="count(procesor) != 1">In pattern count(procesor) != 1:
   element pc musí obsahovat právě jednu položku procesor
</xsl:if>
      <xsl:if test="count(memory) != 1">In pattern count(memory) != 1:
   element pc musí obsahovat právě jednu položku memory
</xsl:if>
      <xsl:if test="count(os) != 1">In pattern count(os) != 1:
   element pc musí obsahovat právě jednu položku os
</xsl:if>
      <xsl:if test="count(graphic) != 1">In pattern count(graphic) != 1:
   element pc musí obsahovat právě jednu položku graphic
</xsl:if>
      <xsl:if test="count(storage) != 1">In pattern count(storage) != 1:
   element pc musí obsahovat právě jednu položku storage
</xsl:if>
      <xsl:if test="count(drives) != 1">In pattern count(drives) != 1:
   element pc musí obsahovat právě jednu položku drives
</xsl:if>
      <xsl:if test="count(battery) != 1">In pattern count(battery) != 1:
   element pc musí obsahovat právě jednu položku battery
</xsl:if>
      <xsl:if test="count(weight) != 1">In pattern count(weight) != 1:
   element pc musí obsahovat právě jednu položku weight
</xsl:if>
      <xsl:if test="count(network) != 1">In pattern count(network) != 1:
   element pc musí obsahovat právě jednu položku network
</xsl:if>
      <xsl:if test="count(accessories) != 1">In pattern count(accessories) != 1:
   element pc musí obsahovat právě jednu položku accesorries
</xsl:if>
      <xsl:if test="count(images) != 1">In pattern count(images) != 1:
   element pc musí obsahovat právě jednu položku images
</xsl:if>
      <xsl:if test="count(video) != 1">In pattern count(video) != 1:
   element pc musí obsahovat právě jednu položku video
</xsl:if>
      <xsl:if test="count(description) != 1">In pattern count(description) != 1:
   element pc musí obsahovat právě jednu položku description
</xsl:if>
      <xsl:if test="count(@id) != 1">In pattern count(@id) != 1:
   element pc musí obsahovat právě jeden unikátní atribut id
</xsl:if>
      <xsl:apply-templates mode="M1"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M1"/>
   <xsl:template match="/notebooks/pc/procesor" priority="4000" mode="M2">
      <xsl:if test="count(p_type) != 1">In pattern count(p_type) != 1:
   element procesor musí obsahovat právě jednu položku p_type
</xsl:if>
      <xsl:if test="count(p_frequence) != 1">In pattern count(p_frequence) != 1:
   element procesor musí obsahovat právě jednu položku p_frequence
</xsl:if>
      <xsl:apply-templates mode="M2"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M2"/>
   <xsl:template match="/notebooks/pc/memory" priority="4000" mode="M3">
      <xsl:if test="count(m_type) != 1">In pattern count(m_type) != 1:
   element memory musí obsahovat právě jednu položku m_type
</xsl:if>
      <xsl:if test="count(m_size) != 1">In pattern count(m_size) != 1:
   element memory musí obsahovat právě jednu položku m_size
</xsl:if>
      <xsl:if test="count(m_frequence) != 1">In pattern count(m_frequence) != 1:
   element memory musí obsahovat právě jednu položku m_frequence
</xsl:if>
      <xsl:apply-templates mode="M3"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M3"/>
   <xsl:template match="/notebooks/pc/os" priority="4000" mode="M4">
      <xsl:if test="count(o_type) != 1">In pattern count(o_type) != 1:
   element os musí obsahovat právě jednu položku o_type
</xsl:if>
      <xsl:if test="count(o_version) != 1">In pattern count(o_version) != 1:
   element os musí obsahovat právě jednu položku o_version
</xsl:if>
      <xsl:apply-templates mode="M4"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M4"/>
   <xsl:template match="/notebooks/pc/graphic" priority="4000" mode="M5">
      <xsl:if test="count(g_card) != 1">In pattern count(g_card) != 1:
   element graphic musí obsahovat právě jednu položku g_card
</xsl:if>
      <xsl:if test="count(g_display) != 1">In pattern count(g_display) != 1:
   element graphic musí obsahovat právě jednu položku g_display
</xsl:if>
      <xsl:apply-templates mode="M5"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M5"/>
   <xsl:template match="/notebooks/pc/storage" priority="4000" mode="M6">
      <xsl:if test="count(s_size) != 1">In pattern count(s_size) != 1:
   element storage musí obsahovat právě jednu položku s_size
</xsl:if>
      <xsl:if test="count(s_count) != 1">In pattern count(s_count) != 1:
   element storage musí obsahovat právě jednu položku s_count
</xsl:if>
      <xsl:apply-templates mode="M6"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M6"/>
   <xsl:template match="/notebooks/pc/drives" priority="4000" mode="M7">
      <xsl:if test="count(drive) &lt; 1">In pattern count(drive) &lt; 1:
   element drives musí obsahovat alespoň jednu položku drive
</xsl:if>
      <xsl:apply-templates mode="M7"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M7"/>
   <xsl:template match="/notebooks/pc/network" priority="4000" mode="M8">
      <xsl:if test="count(adapter) &lt; 0">In pattern count(adapter) &lt; 0:
   element network může obsahovat žádnou anebo libovolný počet položek adapter
</xsl:if>
      <xsl:apply-templates mode="M8"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M8"/>
   <xsl:template match="/notebooks/pc/accessories" priority="4000" mode="M9">
      <xsl:if test="count(accessory) &lt; 0">In pattern count(accessory) &lt; 0:
   element accessories může obsahovat žádnou anebo libovolný počet položek accessory
</xsl:if>
      <xsl:apply-templates mode="M9"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M9"/>
   <xsl:template match="/notebooks/pc/images" priority="4000" mode="M10">
      <xsl:if test="count(image) &lt; 1">In pattern count(image) &lt; 1:
   element images musí obsahovat alespoň jednu položku image
</xsl:if>
      <xsl:apply-templates mode="M10"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M10"/>
   <xsl:template match="/notebooks/pc/description" priority="4000" mode="M11">
      <xsl:if test="count(par) &lt; 1">In pattern count(par) &lt; 1:
   element description musí obsahovat alespoň jednu položku par
</xsl:if>
      <xsl:apply-templates mode="M11"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M11"/>
   <xsl:template match="/notebooks/pc/graphic/g_card" priority="4000" mode="M12">
      <xsl:if test="count(c_name) != 1">In pattern count(c_name) != 1:
   element g_card musí obsahovat právě jednu položku c_name
</xsl:if>
      <xsl:if test="count(c_manufacturer) != 1">In pattern count(c_manufacturer) != 1:
   element g_card musí obsahovat právě jednu položku c_manufacturer
</xsl:if>
      <xsl:if test="count(c_memory_size) != 1">In pattern count(c_memory_size) != 1:
   element g_card musí obsahovat právě jednu položku c_memory_size
</xsl:if>
      <xsl:apply-templates mode="M12"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M12"/>
   <xsl:template match="/notebooks/pc/graphic/g_display" priority="4000" mode="M13">
      <xsl:if test="count(d_technology) != 1">In pattern count(d_technology) != 1:
   element g_display musí obsahovat právě jednu položku d_technology
</xsl:if>
      <xsl:if test="count(d_type) != 1">In pattern count(d_type) != 1:
   element g_display musí obsahovat právě jednu položku d_type
</xsl:if>
      <xsl:if test="count(d_size) != 1">In pattern count(d_size) != 1:
   element g_display musí obsahovat právě jednu položku d_size
</xsl:if>
      <xsl:if test="count(d_resolution) != 1">In pattern count(d_resolution) != 1:
   element g_display musí obsahovat právě jednu položku d_resolution
</xsl:if>
      <xsl:apply-templates mode="M13"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M13"/>
   <xsl:template match="/notebooks/pc/graphic/g_display/d_resolution" priority="4000" mode="M14">
      <xsl:if test="count(r_width) != 1">In pattern count(r_width) != 1:
   element d_resolution musí obsahovat právě jednu položku r_width
</xsl:if>
      <xsl:if test="count(r_height) != 1">In pattern count(r_height) != 1:
   element d_resolution musí obsahovat právě jednu položku r_height
</xsl:if>
      <xsl:apply-templates mode="M14"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M14"/>
   <xsl:template match="/notebooks/pc" priority="4000" mode="M15">
      <xsl:if test="count(../pc[@id = current()/@id]) &gt; 1">In pattern count(../pc[@id = current()/@id]) &gt; 1:
   Duplicitní id:<xsl:text xml:space="preserve"> </xsl:text>
         <xsl:value-of select="@id"/>
         <xsl:text xml:space="preserve"> </xsl:text>u elementu<xsl:text xml:space="preserve"> </xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:text xml:space="preserve"> </xsl:text>.
</xsl:if>
      <xsl:apply-templates mode="M15"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M15"/>
   <xsl:template match="text()" priority="-1"/>
</xsl:stylesheet>