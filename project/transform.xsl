<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/country">
        <xsl:apply-templates select="property" />
    </xsl:template>

    <xsl:template match="property">

        <xsl:variable name="name" select="translate(lower-case(name), ' ', '-')" />

        <xsl:choose>
            <xsl:when test="contains('0123456789', substring($name,1,1))">
                <xsl:variable name="name">x-<xsl:value-of select="$name" /></xsl:variable>
                <xsl:value-of select="$name" />
                <xsl:element name="{$name}">
                    <xsl:value-of select="value" />
                    <xsl:apply-templates select="property" />
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{$name}">
                    <xsl:value-of select="value" />
                    <xsl:apply-templates select="property" />
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>