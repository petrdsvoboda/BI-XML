<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE nation SYSTEM "nation.dtd">
<nation name="Curacao" id="curacao">
	<source url="https://www.cia.gov/library/publications/resources/the-world-factbook/geos/iz.html" />
    <introduction>
        <background>Originally settled by Arawak Indians, Curacao was seized by the Dutch in 1634 along with the neighboring island of Bonaire. Once the center of the Caribbean slave trade, Curacao was hard hit economically by the abolition of slavery in 1863. Its prosperity (and that of neighboring Aruba) was restored in the early 20th century with the construction of the Isla Refineria to service the newly discovered Venezuelan oil fields. In 1954, Curacao and several other Dutch Caribbean possessions were reorganized as the Netherlands Antilles, part of the Kingdom of the Netherlands. In referenda in 2005 and 2009, the citizens of Curacao voted to become a self-governing country within the Kingdom of the Netherlands. The change in status became effective in October 2010 with the dissolution of the Netherlands Antilles.</background>
    </introduction>
    <geography>
        <location>Caribbean, an island in the Caribbean Sea, 55 km off the coast of Venezuela</location>
        <geographic-coordinates>12 10 N, 69 00 W</geographic-coordinates>
        <map-references>Central America and the Caribbean</map-references>
        <area>
            <area-total>444 sq km</area-total>
            <land>444 sq km</land>
            <water>0 sq km</water>
            <country-comparison-to-the-world>200</country-comparison-to-the-world>
        </area>
        <area-comparative>more than twice the size of Washington, DC</area-comparative>
        <land-boundaries>0 km</land-boundaries>
        <coastline>364 km</coastline>
        <maritime-claims>
            <territorial-sea>12 nm</territorial-sea>
            <exclusive-economic-zone>200 nm</exclusive-economic-zone>
        </maritime-claims>
        <climate>tropical marine climate, ameliorated by northeast trade winds, results in mild temperatures; semiarid with average rainfall of 60 cm/year</climate>
        <terrain>generally low, hilly terrain</terrain>
        <elevation>highest point: Mt. Christoffel 372 m
            <mean-elevation>NA</mean-elevation>
            <elevation-extremes>lowest point: Caribbean Sea 0 m</elevation-extremes>
        </elevation>
        <natural-resources>
            <natural-resource>calcium phosphates</natural-resource>
            <natural-resource>aloes</natural-resource>
            <natural-resource>sorghum</natural-resource>
            <natural-resource>peanuts</natural-resource>
            <natural-resource>vegetables</natural-resource>
            <natural-resource>tropical fruit</natural-resource>
        </natural-resources>
        <land-use>
            <land-use-category type="Agricultural land" percentage="10" />
            <land-use-category type="Forest" percentage="0" />
            <land-use-category type="Other" percentage="90" />
        </land-use>
        <irrigated-land>NA</irrigated-land>
        <population-distribution>largest concentration on the island is Willemstad; smaller settlements near the coast can be found throughout the island, particularly in the northwest</population-distribution>
        <natural-hazards>Curacao is south of the Caribbean hurricane belt and is rarely threatened</natural-hazards>
        <environment-current-issues>problems in waste management that threaten environmental sustainability on the island include pollution of marine areas from domestic sewage, inadequate sewage treatment facilities, industrial effluents and agricultural runoff, the management of toxic substances, and ineffective regulations</environment-current-issues>
        <geography-note>Curacao is a part of the Windward Islands (southern) group in the Lesser Antilles</geography-note>
    </geography>
    <people-and-society>
        <population>149,648
            <country-comparison-to-the-world>188</country-comparison-to-the-world>
        </population>
        <nationality>
            <noun>Curacaoan</noun>
            <adjective>Curacaoan; Dutch</adjective>
        </nationality>
        <ethnic-groups>
            <ethnic-group name="Afro-Caribbean" majority="true" />
            <ethnic-group name="Dutch" majority="false" />
            <ethnic-group name="French" majority="false" />
            <ethnic-group name="Latin American" majority="false" />
            <ethnic-group name="East Asian" majority="false" />
            <ethnic-group name="South Asian" majority="false" />
            <ethnic-group name="Jewish" majority="false" />
        </ethnic-groups>
        <languages>
            <language name="Papiamento" percentage="81.2" official="true" />
            <language name="Dutch" percentage="8" official="true" />
            <language name="Spanish" percentage="4" official="false" />
            <language name="English" percentage="2.9" official="true" />
            <language name="Other" percentage="3.9" official="false" />
        </languages>
        <religions>
            <religion name="Roman Catholic" percentage="72.8" />
            <religion name="Pentecostal" percentage="6.6" />
            <religion name="Protestant" percentage="3.2" />
            <religion name="Adventist" percentage="3" />
            <religion name="Jehovah's Witness" percentage="2" />
            <religion name="Evangelical" percentage="1.9" />
            <religion name="Other" percentage="3.8" />
            <religion name="None" percentage="6" />
            <religion name="Unspecified" percentage="0.6" />
        </religions>
        <age>
            <median total="36.1" male="33.5" female="39.7" />
            <age-structure>
                <age-range from="0" to="14" percentage="20">
                    <male amount="15274" />
                    <female amount="14661" />
                </age-range>
                <age-range from="15" to="24" percentage="14.33">
                    <male amount="11170" />
                    <female amount="10280" />
                </age-range>
                <age-range from="25" to="54" percentage="36.87">
                    <male amount="27152" />
                    <female amount="28029" />
                </age-range>
                <age-range from="55" to="64" percentage="13.69">
                    <male amount="8862" />
                    <female amount="11620" />
                </age-range>
                <age-range from="65" percentage="15.1">
                    <male amount="9270" />
                    <female amount="13330" />
                </age-range>
            </age-structure>
        </age>
        <growth-rate percentage="0.4" />
        <birth-rate amount="13.7" unit="births/1,000 population" />
        <death-rate amount="8.4" unit="deaths/1,000 population" />
        <migration-rate amount="-1.3" unit="migrant(s)/1,000 population" />
        <major-urban-areas-population>WILLEMSTAD (capital) 145,000 (2014)</major-urban-areas-population>
        <sex-ratios>
            <sex-ratio type="At birth" amount="1.05 male(s)/female" />
        </sex-ratios>
        <infant-mortality-rate>
            <infant-mortality-rate-total>7.5 deaths/1,000 live births</infant-mortality-rate-total>
            <infant-mortality-rate-male>8.1 deaths/1,000 live births</infant-mortality-rate-male>
            <infant-mortality-rate-female>7 deaths/1,000 live births (2017 est.)</infant-mortality-rate-female>
            <country-comparison-to-the-world>159</country-comparison-to-the-world>
        </infant-mortality-rate>
        <life-expectancy-at-birth>
            <total-population>78.5 years</total-population>
            <expectancy-male>76.2 years</expectancy-male>
            <expectancy-female>80.9 years (2017 est.)</expectancy-female>
            <country-comparison-to-the-world>60</country-comparison-to-the-world>
        </life-expectancy-at-birth>
        <total-fertility-rate>2.04 children born/woman (2017 est.)
            <country-comparison-to-the-world>113</country-comparison-to-the-world>
        </total-fertility-rate>
    </people-and-society>
    <government>
        <country-name>
            <conventional-long-form>none</conventional-long-form>
            <conventional-short-form>Curacao</conventional-short-form>
            <local-long-form>Land Curacao (Dutch); Pais Korsou (Papiamento)</local-long-form>
            <local-short-form>Curacao (Dutch); Korsou (Papiamento)</local-short-form>
            <etymology>the most plausible name derivation is that the island was designated Isla de la Curacion (Spanish meaning "Island of the Cure" or "Island of Healing") or Ilha da Curacao (Portuguese meaning the same) to reflect the locale's function as a recovery stop for sick crewmen</etymology>
        </country-name>
        <government-type>parliamentary democracy</government-type>
        <capital>
            <capital-name>Willemstad</capital-name>
            <geographic-coordinates>12 06 N, 68 55 W</geographic-coordinates>
            <time-difference>UTC-4 (1 hour ahead of Washington, DC, during Standard Time)</time-difference>
        </capital>
        <administrative-divisions>none (part of the Kingdom of the Netherlands)</administrative-divisions>
        <national-holiday>King's Day (birthday of King WILLEM-ALEXANDER), 27 April (1967); note - King's or Queen's Day are observed on the ruling monarch's birthday; celebrated on 26 April if 27 April is a Sunday</national-holiday>
        <constitution>previous 1947, 1955; latest adopted 5 September 2010, entered into force 10 October 2010 (regulates governance of Curacao but is subordinate to the Charter for the Kingdom of the Netherlands); note - in October 2010, with the dissolution of the Netherlands Antilles, Curacao became a constituent country within the Kingdom of the Netherlands (2017)</constitution>
        <legal-system>based on Dutch civil law system with some English common law influence</legal-system>
        <suffrage>18 years of age; universal</suffrage>
        <executive-branch>
            <chief-of-state>King WILLEM-ALEXANDER of the Netherlands (since 30 April 2013); represented by Governor Lucille A. GEORGE-WOUT (since 4 November 2013)</chief-of-state>
            <head-of-government>Prime Minister Eugene RHUGGENAATH (since 29 May 2017)</head-of-government>
            <cabinet>Cabinet appointed by the governor</cabinet>
        </executive-branch>
        <legislative-branch>
            <description>unicameral Estates of Curacao or Staten van Curacao (21 seats; members directly elected by proportional representation vote to serve 4-year terms)</description>
            <elections>last held 28 April 2017 (next to be held on 2021); early elections are being held after Prime Minister Hensley KOEIMAN resigned on 12 February 2017, when the coalition government lost its majority</elections>
            <election-results>percent of vote by party - PAR 23.3%, MAN 20.4%, MFK 19.9%, KdnT 9.4%, PIN 5.3%, PS 5.1%, MP 4.9%, other 11.7%; seats by party - PAR 6, MAN 5, MFK 5, KdnT 2, PIN 1, PS 1, MP 1</election-results>
        </legislative-branch>
        <judicial-branch>
            <highest-court>Joint Court of Justice of Aruba, Curacao, Sint Maarten, and of Bonaire, Sint Eustatitus and Saba or "Joint Court of Justice" (sits as a 3-judge panel); final appeals heard by the Supreme Court, in The Hague, Netherlands</highest-court>
            <judge-selection-and-term-of-office>Joint Court judges appointed by the monarch for life</judge-selection-and-term-of-office>
            <subordinate-courts>first instance courts, appeals court; specialized courts</subordinate-courts>
        </judicial-branch>
        <political-parties-and-leaders>Un Korsou Hustu [Omayra LEEFLANG]</political-parties-and-leaders>
        <flag-description>on a blue field a horizontal yellow band somewhat below the center divides the flag into proportions of 5:1:2; two five-pointed white stars - the smaller above and to the left of the larger - appear in the canton; the blue of the upper and lower sections symbolizes the sky and sea respectively; yellow represents the sun; the stars symbolize Curacao and its uninhabited smaller sister island of Klein Curacao; the five star points signify the five continents from which Curacao's people derive</flag-description>
        <national-symbol>laraha (citrus tree); national colors: blue, yellow, white</national-symbol>
        <national-anthem>
            <anthem-name>Himmo di Korsou (Anthem of Curacao)</anthem-name>
            <music>Guillermo ROSARIO, Mae HENRIQUEZ, Enrique MULLER, Betty DORAN/Frater Candidus NOWENS, Errol "El Toro" COLINA</music>
            <anthem-note>adapted 1978; the lyrics, originally written in 1899, were rewritten in 1978 to make them less colonial in nature</anthem-note>
        </national-anthem>
    </government>
    <economy>
        <economy-overview>The government is attempting to diversify its industry and trade. Curacao is an Overseas Countries and Territories (OCT) of the European Union. Nationals of Curacao are citizens of the European Union, even though it is not a member. Based on its OCT status, products that originate in Curacao have preferential access to the EU and are exempt from import duties. Curacao is a beneficiary of the Caribbean Basin Initiative and, as a result, products originating in Curacao can be imported tax free into the US if at least 35% has been added to the value of these products in Curacao. The island has state-of-the-art information and communication technology connectivity with the rest of the world, including a Tier IV datacenter. With several direct satellite and submarine optic fiber cables, Curacao has one of the best Internet speeds and reliability in the Western Hemisphere.</economy-overview>
        <gdp-purchasing-power-parity>$2.96 billion (2010 est.)
            <country-comparison-to-the-world>188</country-comparison-to-the-world>
        </gdp-purchasing-power-parity>
        <gdp-official-exchange-rate>$5.6 billion (2012 est.)</gdp-official-exchange-rate>
        <gdp-real-growth-rate>0.1% (2010 est.)
            <country-comparison-to-the-world>88</country-comparison-to-the-world>
        </gdp-real-growth-rate>
        <gdp-per-capita>$15,000 (2004 est.)
            <country-comparison-to-the-world>111</country-comparison-to-the-world>
        </gdp-per-capita>
        <by-end-use>
            <household-consumption>66.9%</household-consumption>
            <government-consumption>33.6%</government-consumption>
            <investment-in-fixed-capital>19.4%</investment-in-fixed-capital>
            <investment-in-inventories>0%</investment-in-inventories>
            <exports-of-goods-and-services>17.5%</exports-of-goods-and-services>
            <imports-of-goods-and-services>-37.5% (2016 est.)</imports-of-goods-and-services>
        </by-end-use>
        <by-sector-of-origin>
            <agriculture>0.7%</agriculture>
            <industry>15.5%</industry>
            <services>83.8% (2012 est.)</services>
        </by-sector-of-origin>
        <agriculture-products>
            <agriculture-product>aloe</agriculture-product>
            <agriculture-product>sorghum</agriculture-product>
            <agriculture-product>peanuts</agriculture-product>
            <agriculture-product>vegetables</agriculture-product>
            <agriculture-product> tropical fruit</agriculture-product>
        </agriculture-products>
        <industries>
            <industry>tourism</industry>
            <industry>petroleum refining</industry>
            <industry>petroleum transshipment</industry>
            <industry>light manufacturing</industry>
            <industry>financial and business services</industry>
        </industries>
        <industrial-production-growth-rate>NA</industrial-production-growth-rate>
        <labor-force>73,010 (2013)
            <country-comparison-to-the-world>185</country-comparison-to-the-world>
        </labor-force>
        <labor-force-by-occupation>
            <agriculture>1.2%</agriculture>
            <industry>16.9%</industry>
            <services>81.8% (2008 est.)</services>
        </labor-force-by-occupation>
        <unemployment-rate>9.8% (2011 est.)
            <country-comparison-to-the-world>161</country-comparison-to-the-world>
        </unemployment-rate>
        <taxes-and-other-revenues>16.6% of GDP (2012 est.)
            <country-comparison-to-the-world>180</country-comparison-to-the-world>
        </taxes-and-other-revenues>
        <public-debt>40.6% of GDP (2011 est.)
            <country-comparison-to-the-world>150</country-comparison-to-the-world>
        </public-debt>
        <inflation-rate-consumer-prices>2.8% (2012 est.)
            <country-comparison-to-the-world>121</country-comparison-to-the-world>
        </inflation-rate-consumer-prices>
        <exports>$1.44 billion (2010 est.)
            <country-comparison-to-the-world>148</country-comparison-to-the-world>
        </exports>
        <exports-commodities>petroleum products</exports-commodities>
        <imports>$1.275 billion (2010 est.)
            <country-comparison-to-the-world>171</country-comparison-to-the-world>
        </imports>
        <imports-commodities>crude petroleum, food, manufactures</imports-commodities>
        <exchange-rates>1.79 (2013 est.)</exchange-rates>
    </economy>
    <energy>
        <electricity-production>1.785 billion kWh (2012 est.)
            <country-comparison-to-the-world>142</country-comparison-to-the-world>
        </electricity-production>
        <electricity-consumption>968 million kWh (2008 est.)
            <country-comparison-to-the-world>156</country-comparison-to-the-world>
        </electricity-consumption>
        <electricity-exports>0 kWh (2009 est.)
            <country-comparison-to-the-world>116</country-comparison-to-the-world>
        </electricity-exports>
        <electricity-imports>0 kWh (2009 est.)
            <country-comparison-to-the-world>130</country-comparison-to-the-world>
        </electricity-imports>
        <crude-oil-proved-reserves>0 bbl (1 January 2011 es)
            <country-comparison-to-the-world>124</country-comparison-to-the-world>
        </crude-oil-proved-reserves>
        <refined-petroleum-products-production>531.1 bbl/day (2010 est.)
            <country-comparison-to-the-world>106</country-comparison-to-the-world>
        </refined-petroleum-products-production>
        <refined-petroleum-products-consumption>72,000 bbl/day (2010 est.)
            <country-comparison-to-the-world>91</country-comparison-to-the-world>
        </refined-petroleum-products-consumption>
        <refined-petroleum-products-exports>211,100 bbl/day (2009 est.)
            <country-comparison-to-the-world>33</country-comparison-to-the-world>
        </refined-petroleum-products-exports>
        <refined-petroleum-products-imports>291,700 bbl/day (2009 est.)
            <country-comparison-to-the-world>27</country-comparison-to-the-world>
        </refined-petroleum-products-imports>
        <natural-gas-production>0 cu m (2009 est.)
            <country-comparison-to-the-world>117</country-comparison-to-the-world>
        </natural-gas-production>
        <natural-gas-consumption>0 cu m (2009 est.)
            <country-comparison-to-the-world>165</country-comparison-to-the-world>
        </natural-gas-consumption>
        <natural-gas-exports>0 cu m (2009 est.)
            <country-comparison-to-the-world>79</country-comparison-to-the-world>
        </natural-gas-exports>
        <natural-gas-imports>0 cu m (2009 est.)
            <country-comparison-to-the-world>102</country-comparison-to-the-world>
        </natural-gas-imports>
        <natural-gas-proved-reserves>0 cu m (1 January 2011 es)
            <country-comparison-to-the-world>123</country-comparison-to-the-world>
        </natural-gas-proved-reserves>
    </energy>
    <communications>
        <telephone-system>country code - 599</telephone-system>
        <broadcast-media>government-run TeleCuracao operates a TV station and a radio station; 3 other privately owned TV stations and several privately owned radio stations (2017)</broadcast-media>
        <internet-country-code>.cw</internet-country-code>
        <internet-users>
            <total-users>138,750</total-users>
            <percent-of-population>93.9% (July 2016 est.)</percent-of-population>
        </internet-users>
    </communications>
    <transportation>
        <national-air-transport-system>
            <number-of-registered-air-carriers>2</number-of-registered-air-carriers>
            <inventory-of-registered-aircraft-operated-by-air-carriers>11 (2015)</inventory-of-registered-aircraft-operated-by-air-carriers>
        </national-air-transport-system>
        <civil-aircraft-registration-country-code-prefix>PJ (2016)</civil-aircraft-registration-country-code-prefix>
        <airports>1 (2017)</airports>
        <airports-with-paved-runways>
            <airport type="047 m" amount="1 (2017)" />
        </airports-with-paved-runways>
        <roadways>
            <roadways-total>550 km</roadways-total>
            <country-comparison-to-the-world>194</country-comparison-to-the-world>
        </roadways>
        <merchant-marine>
            <merchant-total>92</merchant-total>
            <by-type>general cargo 18, oil tanker 1, other 73 (2017)</by-type>
        </merchant-marine>
        <ports-and-terminals>
            <major-seaport>Willemstad</major-seaport>
            <oil-terminal>Bullen Baai (Curacao Terminal)</oil-terminal>
            <bulk-cargo-port>Fuik Bay (phosphate rock)</bulk-cargo-port>
        </ports-and-terminals>
    </transportation>
    <military-and-security>
        <military-branches>no regular military forces; the Dutch Government controls foreign and defense policy (2012)</military-branches>
        <military-service-age-and-obligation>no conscription (2010)</military-service-age-and-obligation>
    </military-and-security>
</nation>