#!/bin/bash
saxonb-xslt -xsl:nation-epub.xsl -it:main -ext:on -o:epub/contents.html
cd epub
zip -X0 nations.epub mimetype
zip -Xur9D nations.epub *
cd ..
mv epub/nations.epub nations.epub