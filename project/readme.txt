Required software
xmllint, saxon-xslt(java), apache fop, zip, trang

Build all parts by running build.sh

Generated html file with contents: main.html
Generated pdf file: nations.pdf
Generated epub book: nations.epub

Otherwise:
Linting:
xmllint --noout --dtdvalid nation.dtd nation-name.xml (DTD)
trang nation.rnc nation.rng
xmllint --noout --relaxng nation.rng nation-name.xml (DTD)
Build html:
saxonb-xslt -xsl:nation.xsl -it:main -ext:on -o:main.html
Build pdf:
fop -xml nations.xml -xsl nation-fo.xsl -pdf nations.pdf
Build epub:
saxonb-xslt -xsl:nation-epub.xsl -it:main -ext:on -o:epub/contents.html
cd epub
zip -X0 nations.epub mimetype
zip -Xur9D nations.epub *
cd ..
mv epub/nations.epub nations.epub

Folders:
images - required for html and pdf output
epub - all misc files required for epub output