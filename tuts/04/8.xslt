<xsl:stylesheet version = '3.0' 
	xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:output omit-xml-declaration="yes" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates select="/a"/>
	</xsl:template>

	<xsl:template match="/a">
		<ul>
			<xsl:apply-templates select="/a/*"/>
		</ul>
	</xsl:template>
	<xsl:template match="/a/*">
		<li>
			<xsl:value-of select="."/>
		</li>
	</xsl:template>
</xsl:stylesheet>