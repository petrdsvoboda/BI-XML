<xsl:stylesheet version = '3.0' 
	xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:output omit-xml-declaration="yes" indent="yes"/>

	<xsl:template match="/x">
		<ul>
			<xsl:apply-templates select="y" mode="TOC" />
		</ul>
		<xsl:apply-templates select="y" mode="cont" />
	</xsl:template>

	<xsl:template match="y" mode="TOC">
		<li>
			<a href="./{generate-id()}.html">
				<xsl:value-of select="."/>
			</a>
		</li>
	</xsl:template>
	
	<xsl:template match="y" mode="cont">
		<xsl:result-document href="./{generate-id()}.html">
			<p>
				text <xsl:value-of select="."/>
			</p>
		</xsl:result-document>
	</xsl:template>
</xsl:stylesheet>