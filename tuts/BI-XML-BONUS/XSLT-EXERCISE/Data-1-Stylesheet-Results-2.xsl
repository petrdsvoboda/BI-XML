<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="1.0">

<xsl:output method="html"/>

<xsl:template match="/article">
    <html>
        <head>
            <title><xsl:value-of select="./articleinfo/title"/></title>
            <style type="text/css">
                body{font-family:Sans-serif;}
                li.S1{font-size:110%;}
                li.S2{font-size:90%;margin-left:30px;}
            </style>
        </head>
        <body>
            <h1><xsl:value-of select="./articleinfo/title"/></h1>
            <p>
                <b><xsl:value-of select="./articleinfo/publisher/publishername"/></b>
                <xsl:text> </xsl:text>
                <xsl:value-of select="./articleinfo/pubdate"/>
                <xsl:text>, </xsl:text>
                <b><xsl:value-of select="./articleinfo/volumenum"/></b><xsl:text>:</xsl:text>
                <xsl:value-of select="./articleinfo/issuenum"/>
            </p>
            <xsl:for-each select="//sect1|//sect2">
                    <xsl:apply-templates select="./title">
                        <xsl:with-param name="number"><xsl:number format="1"/>. </xsl:with-param>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="./para|./itemizedlist"/>
            </xsl:for-each>
            <h2>Contents</h2>
            <ul>
                <xsl:for-each select="//sect1|//sect2">
                    <xsl:apply-templates select="./title" mode="toc">
                        <xsl:with-param name="number"><xsl:number format="1"/>. </xsl:with-param>
                    </xsl:apply-templates>
                </xsl:for-each>
            </ul>
            <h2>Links</h2>
            <ul>
                <xsl:for-each select="//ulink">
                    <xsl:sort data-type="text" case-order="upper-first" select="."/>
                    <li><xsl:apply-templates select="."/></li>
                </xsl:for-each>
            </ul>
        </body>
    </html>
</xsl:template>

<xsl:template match="title" mode="toc">
    <xsl:param name="number"><xsl:number format="1"/>. </xsl:param>
    <xsl:choose>
        <xsl:when test=".[parent::sect1]">
            <li class="S1"><a href="#{.}"><xsl:value-of select="$number"/><xsl:value-of select="."/></a></li>
        </xsl:when>
        <xsl:otherwise>
            <li class="S2"><a href="#{.}"><xsl:value-of select="$number"/><xsl:value-of select="."/></a></li>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="title">
    <xsl:param name="number"><xsl:number format="1"/>. </xsl:param>
    <xsl:choose>
        <xsl:when test=".[parent::sect1]">
            <h2><a name="{.}"><xsl:value-of select="$number"/><xsl:value-of select="."/></a></h2>
        </xsl:when>
        <xsl:otherwise>
            <h3><a name="{.}"><xsl:value-of select="$number"/><xsl:value-of select="."/></a></h3>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="itemizedlist">
    <ul><xsl:apply-templates select="./listitem"/></ul>
</xsl:template>

<xsl:template match="listitem">
    <li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="para">
    <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="ulink">
    <a href="{@url}"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="email|quote">
    <i><xsl:value-of select="."/></i>
</xsl:template>

</xsl:stylesheet>
